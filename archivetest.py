'''
Created on Jun 11, 2014

@author: hauyeung
'''
import unittest,os, archiver, shutil,zipfile


class Test(unittest.TestCase):
    def setUp(self):
        os.makedirs('testdir')  
        os.makedirs('testarchive')
        os.chdir('testarchive')
        for x in range(7):    
            open('file'+str(x),'a').close()
        os.chdir(os.path.dirname(os.path.realpath(__file__)))
        self.testarchive = archiver.createarchive('testarchive')
        self.testdir = archiver.createarchive('testdir')
                                
    def test_archive(self):
        os.chdir(os.path.dirname(os.path.realpath(__file__)))
        self.assertEqual(7,self.testarchive)
            
        
    def test_empty(self):      
        os.chdir(os.path.dirname(os.path.realpath(__file__)))  
        self.assertEqual(0, self.testdir)
        
    def test_filetype(self):
        os.chdir(os.path.dirname(os.path.realpath(__file__)))
        self.assertEqual(zipfile.is_zipfile('testarchive.zip'), True)        
        self.assertEqual(zipfile.is_zipfile('testdir.zip'), True)
        
    def test_content(self):  
        os.chdir(os.path.dirname(os.path.realpath(__file__)))   
        fileslist = zipfile.ZipFile('testarchive.zip','r').namelist()
        correctfileslist = []
        for x in range(7):
            correctfileslist.append('file'+str(x))
        self.assertEqual(set(correctfileslist), set(fileslist))
        
    
    def tearDown(self):
        os.chdir(os.path.dirname(os.path.realpath(__file__)))    
        shutil.rmtree('testdir')
        shutil.rmtree('testarchive')
        os.remove('testarchive.zip')
        os.remove('testdir.zip')



if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()