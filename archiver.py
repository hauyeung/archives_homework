'''
Created on Jun 10, 2014

@author: hauyeung
'''
import os, zipfile

def createarchive(path):
    os.chdir(os.path.dirname(os.path.realpath(__file__)))
    pathnames = path.split('/')
    filename = pathnames[0]
    zip = zipfile.ZipFile(filename+'.zip','w')
    files = os.listdir(path)
    os.chdir(path)
    for file in files:
        if os.path.isfile(path,file):            
            zip.write(file)
    zip.close()
    return len(zip.namelist())
            
